def make_sandwich(*args):
    print('making a sandwich')
    print('bread')
    for thing in args:
        print(thing)
    print('bread')

make_sandwich('bacon','lettuce','tomato')
make_sandwich('peanut butter','jelly')
make_sandwich('meatloaf')
