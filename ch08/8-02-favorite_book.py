#!/usr/bin/env python3

def favorite_book(title):
    print(f"One of my favorite books is { title }.")

favorite_book("The Martian")
favorite_book("Python Crash Course")
favorite_book("Continuous Delivery")
favorite_book("Measure What Matters")
favorite_book("Making Work Visible")
