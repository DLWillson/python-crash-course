def make_car(make, model, **other_attributes):
    attributes=other_attributes
    attributes['make']=make.title()
    attributes['model']=model.title()
    return(attributes)
