def describe_city(city,country="United States"):
    print(f'{city} is in {country}.')

describe_city('Rochester')
describe_city('Reykjavik','Iceland')
describe_city('London','England')
