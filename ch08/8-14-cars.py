def make_car(make, model, **other_attributes):
    attributes=other_attributes
    attributes['make']=make.title()
    attributes['model']=model.title()
    return(attributes)

subaru = make_car('subaru', 'outback', color='blue', tow_package=True)
honky = make_car('volkswagen', 'eurovan', color='white', year='1993')
frog = make_car('ford', 'explorer', color='green', style='comfortable')

print(subaru)
print('---')
print(honky)
print('---')
print(frog)
