def make_album(artist,title):
    album={
    'artist': artist.title(),
    'title': title.title(),
    }
    return(album)

while True:
    print("Give me artists and titles with which I make albums.")
    print("Enter <q> at any time to quit.")

    artist=input('artist: ')
    if artist=='q':
        break
    title=input('title: ')
    if title=='q':
        break
    print(make_album(artist,title))
