def format_city(city,country):
    return(f'{city.title()}, {country.title()}')

print(format_city('rochester','New York'))
print(format_city('reykjavik','iceland'))
print(format_city('london','england'))
