def make_album(artist,title):
    album={
    'artist': artist.title(),
    'title': title.title(),
    }
    return(album)

album1=make_album('beatles','the black album')
print(f'album1 is a {type(album1)}.')
print(album1)
print(make_album('megadeth',"peace sells, but who's buying?"))
print(make_album('Savatage',"Hall of the Mountain King"))
