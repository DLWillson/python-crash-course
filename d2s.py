#!/usr/bin/env python3

def dict_to_string(dict):
    string_from_dict="too bad dicts don't store a title:\n"
    for key, value in dict.items():
        string_from_dict += "- " + key + ": " + str(value) + "\n"
    return string_from_dict

one_fruit = {
    "david":"nectarine",
    "corey":"huckelberry",
    "rich":"peach",
    "alison":"blueberries",
}

two_fruit = {
    "david": [
        "nectarine",
        "blueberry",
    ],
    "corey": [
        "huckelberry",
        "blackberry",
    ],
    "rich": [
        "peach",
        "nectarine",
    ],
    "alison": [
        "blueberries",
        "bananas",
    ],
}

red_fruit = {
    "suess": 'apple',
    "sendak": [
        'tomato',
        'cherry',
    ],
    "silverstein": [
        'raspberry',
        'strawberry',
        'rhubarb',
    ],
}

blue_fruit = {
    "cold miser": [
        'blueberries',
        'acai',
        'corn',
    ],
    "mr freeze": 'grapes',
}

print(dict_to_string(one_fruit))
print("---\n")
print(dict_to_string(two_fruit))
print("---\n")
print(dict_to_string(red_fruit))
print("---\n")
print(dict_to_string(blue_fruit))
