# How to Prepare a Lesson

[Online](https://gitlab.com/DLWillson/python-crash-course/-/blob/master/ch02/how-to-prep-a-lesson.md)

Plan to deliver the lesson in Time Pie or another form that allows learners to hear and see, do and say.

A 1 Hour Time Pie is:
+ 15 min [TED-style Talk](https://speakupforsuccess.com/how-are-ted-talks-and-business-presentations-different/)
+ 15 min Demo
+ 30 min Pair Play

---

1. Read the chapter and do its exercises, noting things you might want to include in your lesson. (varies)
2. Write the lesson plan (one hour)
  * Title
  * Objectives: What are the 1/3/7 most important things to learn?
  * Exercises:  Choose or create some exercises that match your Objectives.
3. Rehearse the lesson (one hour)
4. Revise the plan (one hour)
5. Rehearse the lesson (one hour)
6. Revise the plan (one hour)
7. Deliver in Time Pie (one hour)
  * Talk for 25% : Do a TED Talk that covers your Objectives
  * Demo for 25% : Do your Exercises explaining as you go along.
  * Help for 50% : Help the small groups get through the exercise/s.

---

Some Don'ts

The dos are all above. They prevent the don'ts, if you do them. But, they don't if you don't. So, these don'ts are here as a caution.

- Don't under-estimate the time it takes to prep. Baking, roasting, and prepping a lesson take time.
- Don't spend more than an hour at each writing, because you'll write wrong, as if for a book, not a live class.
- Don't have more than seven objectives, or you'll meander, be boring, fail to meet your goals, and/or run over time.
- Don't be boring. Don't blather or chat. Don't go off script. Don't make unqualified guesses.
- Don't be naively optimistic and ignorantly assume the exercises will go well for the students.
- Don't write your lesson plan as you go over someone else's material. Just take notes.
