class Restaurant:
    """ A Restaurant with Class """

    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine
        self.num_served = 0

    def info(self):
        print(f"{self.name} specializes in {self.cuisine} cuisine.")

    def open(self):
        print(f"{self.name} is now open for business!")

    def set_num_served(self, num_served):
        self.num_served = num_served

    def increment_num_served(self, num_served):
        self.num_served += num_served

    def get_num_served(self):
        print(f"{self.name} has served {self.num_served} people.")

class IceCreamStand(Restaurant):
    """ a specific kind of restaurant """

    def __init__(self, name, cuisine, flavors):
        super().__init__(name, cuisine)
        self.flavors = flavors

    def get_flavors(self):
        print(f"{self.name} has {self.flavors} flavors.")
