# 9-8. Privileges: Write a separate Privileges class. The class should have one attribute, privileges, that stores a list of strings as described in Exercise 9-7. Move the show_privileges() method to this class. Make a Privileges instance as an attribute in the Admin class. Create a new instance of Admin and use your method to show its privileges.

# Matthes, Eric. Python Crash Course, 2nd Edition (pp. 384-385). No Starch Press. Kindle Edition.

class User(object):
    """docstring for User."""

    def __init__(self, first_name, last_name):
        super(User, self).__init__()
        self.first_name = first_name
        self.last_name = last_name
        self.login_attempts = 0

    def info(self):
        print(f"{self.first_name} {self.last_name} is swell.")

    def greet(self):
        print(f"Hello Mr or Mrs {self.first_name} {self.last_name}.")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts=0

    def get_login_attempts(self):
        print(f"{self.first_name} has attempted login {self.login_attempts} times.")
