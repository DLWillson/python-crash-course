from random import choice
import sys

balls=set(range(10))
balls.add('a')
balls.add('b')
balls.add('c')
balls.add('d')
balls.add('e')
balls.add('f')

blanks=tuple(balls)

# print(blanks)

winner=set()
print("Get ready, Colorado, we're about to choose the winning lottery numbers!")

for pos in range(4):
    t=choice(list(balls))
    balls.remove(t)
    print(f"'{t}'!")
    winner.add(str(t))

winner=sorted(winner)

print(f"If your lottery ticket reads: {winner}, you're a winner (who probably doesn't understand probability)!")

attempts=0
guess=set()
while True:
    attempts += 1
    ttblanks=set(blanks)
    guess=set()
    # print(f"guess is currently a {type(guess)}")
    for pos in range(4):
        t=choice(list(ttblanks))
        ttblanks.remove(t)
        # print(f"guess is currently a {type(guess)}")
        guess.add(str(t))
    if sorted(guess) == winner:
        # print(f"{sorted(guess)} == {winner}")
        print("WINNER!")
        break
    else:
        # print(f"{sorted(guess)} != {winner}")
        sys.stdout.write('.')

print(f"Winner was found in {attempts} attempts!")
