class User(object):
    """docstring for User."""

    def __init__(self, first_name, last_name):
        super(User, self).__init__()
        self.first_name = first_name
        self.last_name = last_name
        self.login_attempts = 0

    def info(self):
        print(f"{self.first_name} {self.last_name} is swell.")

    def greet(self):
        print(f"Hello Mr or Mrs {self.first_name} {self.last_name}.")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts=0

    def get_login_attempts(self):
        print(f"{self.first_name} has attempted login {self.login_attempts} times.")


david = User("David", "Willson")
rich = User("Rich", "Glazier")
corey = User("Corey", "MacDonald")
dakota = User("Dakota", "LaBarr")
shawna = User("Shawna", "Saunders")

david.info()
david.greet()

rich.info()
rich.greet()

corey.info()
corey.greet()

dakota.info()
dakota.greet()

shawna.info()
shawna.greet()

corey.increment_login_attempts()
corey.increment_login_attempts()
corey.increment_login_attempts()
corey.get_login_attempts()
corey.reset_login_attempts()
corey.get_login_attempts()
