class Restaurant:
    """ A Restaurant with Class """

    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine

    def info(self):
        print(f"{self.name} specializes in {self.cuisine} cuisine.")

    def open(self):
        print(f"{self.name} is now open for business!")

restaurant = Restaurant('Burger King', 'American Fast Food')

print(f"The restaurant's name is {restaurant.name}.")
print(f"The restaurant's cuisine is {restaurant.cuisine}.")

restaurant.info()

restaurant.open()

southern_sun = Restaurant('Southern Sun', 'Classic American')

yak_n_yeti = Restaurant('Yak & Yeti', 'Indian')

southern_sun.info()

yak_n_yeti.info()
