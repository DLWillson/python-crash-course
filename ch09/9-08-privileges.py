# 9-8. Privileges: Write a separate Privileges class. The class should have one attribute, privileges, that stores a list of strings as described in Exercise 9-7. Move the show_privileges() method to this class. Make a Privileges instance as an attribute in the Admin class. Create a new instance of Admin and use your method to show its privileges.

# Matthes, Eric. Python Crash Course, 2nd Edition (pp. 384-385). No Starch Press. Kindle Edition.

class User(object):
    """docstring for User."""

    def __init__(self, first_name, last_name):
        super(User, self).__init__()
        self.first_name = first_name
        self.last_name = last_name
        self.login_attempts = 0

    def info(self):
        print(f"{self.first_name} {self.last_name} is swell.")

    def greet(self):
        print(f"Hello Mr or Mrs {self.first_name} {self.last_name}.")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts=0

    def get_login_attempts(self):
        print(f"{self.first_name} has attempted login {self.login_attempts} times.")

class Privileges:
    def __init__(self,privileges):
        self.privileges = privileges
    def show_privileges(self):
        for privilege in self.privileges:
            print(f'- {privilege}')

class Admin(User):
    """ docstring for Admin """

    def __init__(self, first_name, last_name, privileges):
        super().__init__(first_name,last_name)
        self.privileges = Privileges(privileges)

    def get_privileges(self):
        print(f"{self.first_name} has the following privileges:")
        self.privileges.show_privileges()

david = Admin("David", "Willson", ["create user accounts","disable/enable user accounts", "delete user accounts"])
rich = User("Rich", "Glazier")
corey = User("Corey", "MacDonald")
dakota = User("Dakota", "LaBarr")
shawna = User("Shawna", "Saunders")

david.info()
david.greet()

corey.increment_login_attempts()
corey.increment_login_attempts()
corey.increment_login_attempts()
corey.get_login_attempts()
corey.reset_login_attempts()
corey.get_login_attempts()

david.get_privileges()
