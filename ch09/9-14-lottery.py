from random import choice

balls=set(range(10))
balls.add('a')
balls.add('b')
balls.add('c')
balls.add('d')
balls.add('e')
balls.add('f')

winner=set()
print("Get ready, Colorado, we're about to choose the winning lottery numbers!")

for pos in range(4):
    t=choice(list(balls))
    balls.remove(t)
    print(f"'{t}'!")
    winner.add(str(t))

winner=sorted(winner)

print(f"If your lottery ticket reads: {winner}, you're a winner (who probably doesn't understand probability)!")
