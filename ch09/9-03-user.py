class User:
    """docstring for User."""

    def __init__(self, first_name, last_name):
        super(User, self).__init__()
        self.first_name = first_name
        self.last_name = last_name

    def info(self):
        print(f"{self.first_name} {self.last_name} is swell.")

    def greet(self):
        print(f"Hello Mr or Mrs {self.first_name} {self.last_name}.")


david = User("David", "Willson")
rich = User("Rich", "Glazier")
corey = User("Corey", "MacDonald")
dakota = User("Dakota", "LaBarr")
shawna = User("Shawna", "Saunders")

david.info()
david.greet()

rich.info()
rich.greet()

corey.info()
corey.greet()

dakota.info()
dakota.greet()

shawna.info()
shawna.greet()
