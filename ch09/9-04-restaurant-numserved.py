class Restaurant:
    """ A Restaurant with Class """

    def __init__(self, name, cuisine):
        self.name = name
        self.cuisine = cuisine
        self.num_served = 0

    def info(self):
        print(f"{self.name} specializes in {self.cuisine} cuisine.")

    def open(self):
        print(f"{self.name} is now open for business!")

    def set_num_served(self, num_served):
        self.num_served = num_served

    def increment_num_served(self, num_served):
        self.num_served += num_served

    def get_num_served(self):
        print(f"{self.name} has served {self.num_served} people.")

BK = Restaurant('Burger King', 'American Fast Food')

print(f"The restaurant's name is {BK.name}.")
print(f"The restaurant's cuisine is {BK.cuisine}.")

BK.info()

BK.open()

BK.set_num_served(10000)
BK.get_num_served()
BK.increment_num_served(23)
BK.get_num_served()

southern_sun = Restaurant('Southern Sun', 'Classic American')

yak_n_yeti = Restaurant('Yak & Yeti', 'Indian')

southern_sun.info()

yak_n_yeti.info()
