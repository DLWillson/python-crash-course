from users import User

class Privileges:
    def __init__(self,privileges):
        self.privileges = privileges
    def show_privileges(self):
        for privilege in self.privileges:
            print(f'- {privilege}')

class Admin(User):
    """ docstring for Admin """

    def __init__(self, first_name, last_name, privileges):
        super().__init__(first_name,last_name)
        self.privileges = Privileges(privileges)

    def get_privileges(self):
        print(f"{self.first_name} has the following privileges:")
        self.privileges.show_privileges()
