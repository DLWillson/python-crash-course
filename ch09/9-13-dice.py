from random import randint
from pprint import pprint

class Die:
    def __init__(self,sides=6):
        self.sides=sides

    def roll(self):
        return randint(1,self.sides)

normal_die=Die() # this uses the default value of six
ten_sider=Die(10)
twenty_sider=Die(20)

rolls={
    'sixxer': [],
    'tenner': [],
    'twenty': [],
}

for roll in range(10):
    rolls['sixxer'].append(normal_die.roll())
    rolls['tenner'].append(ten_sider.roll())
    rolls['twenty'].append(twenty_sider.roll())

pprint(rolls)
