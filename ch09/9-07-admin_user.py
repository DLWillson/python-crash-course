# 9-7. Admin: An administrator is a special kind of user. Write a class called Admin that inherits from the User class you wrote in Exercise 9-3 (page 162) or Exercise 9-5 (page 167). Add an attribute, privileges, that stores a list of strings like "can add post", "can delete post", "can ban user", and so on. Write a method called show_privileges() that lists the administrator’s set of privileges. Create an instance of Admin, and call your method.

class User(object):
    """docstring for User."""

    def __init__(self, first_name, last_name):
        super(User, self).__init__()
        self.first_name = first_name
        self.last_name = last_name
        self.login_attempts = 0

    def info(self):
        print(f"{self.first_name} {self.last_name} is swell.")

    def greet(self):
        print(f"Hello Mr or Mrs {self.first_name} {self.last_name}.")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts=0

    def get_login_attempts(self):
        print(f"{self.first_name} has attempted login {self.login_attempts} times.")

class Admin(User):
    """ docstring for Admin """

    def __init__(self, first_name, last_name, privileges):
        super().__init__(first_name,last_name)
        self.privileges = privileges

    def get_privileges(self):
        print(f"{self.first_name} has the following privileges:")
        for privilege in self.privileges:
            print(f'- {privilege}')

david = Admin("David", "Willson", ["create user accounts","disable/enable user accounts", "delete user accounts"])
rich = User("Rich", "Glazier")
corey = User("Corey", "MacDonald")
dakota = User("Dakota", "LaBarr")
shawna = User("Shawna", "Saunders")

david.info()
david.greet()

corey.increment_login_attempts()
corey.increment_login_attempts()
corey.increment_login_attempts()
corey.get_login_attempts()
corey.reset_login_attempts()
corey.get_login_attempts()

david.get_privileges()
