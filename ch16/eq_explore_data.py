import json

# explore the structure of the data
filename = 'json-samples/eq_data_1_da_m1.json'
with open(filename) as f:
    all_eq_data = json.read(f)

human_readable_json_file = 'human_readable_eq_data.json'
with open(human_readable_json_file, 'w') as f:
    json.dump(all_eq_data, f, indent=4)
#!/usr/bin/env python3
# Credit: Based on examples in Python Crash Course by Eric H. Matthes
# Author: David L. Willson
# License: GPL for code, CC BY SA for art, or public domain for trivial works
