from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .models import Article
from .forms import ArticleForm

def index(request):
    """Show all Topics"""
    articles = Article.objects.order_by('date_added')
    context = {'articles': articles}
    return render(request, 'index.html', context)

@login_required
def new(request):
    """Add a new article"""
    if request.method != 'POST':
        # no data submitted, create a black form
        form = ArticleForm()
    else:
        # form submitted, process the submission
        form = ArticleForm(data = request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.author = request.user
            article.save()
        return redirect('articles:index')
    
    # Display a blank or invalid form
    context = {'form': form }
    return render(request, 'new.html', context)

@login_required
def edit(request, article_id):

    article = Article.objects.get(id = article_id)
    if article.author != request.user:
        return redirect('articles:index')

    if request.method != 'POST':
        # no data submitted, create a black form
        form = ArticleForm(instance = article)
    else:
        # form submitted, process the submission
        form = ArticleForm(instance = article, data = request.POST)
        if form.is_valid():
            form.save()
        return redirect('articles:index')
    
    # Display a blank or invalid form
    context = {'article': article, 'form': form }
    return render(request, 'edit.html', context)
