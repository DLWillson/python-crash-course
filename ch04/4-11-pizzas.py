my_pizzas=[
    'keep tahoe blue',
    'red hot mama',
    'moses'
]

yo_pizzas=my_pizzas[:]

my_pizzas.append("supreme")

yo_pizzas.append("anchovy")

print("my favorite pizzas:")
for pizza in my_pizzas:
    print(f"- {pizza.title()}")

print("yo favorite pizzas:")
for pizza in yo_pizzas:
    print(f"- {pizza.title()}")
