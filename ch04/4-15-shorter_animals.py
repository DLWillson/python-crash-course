animals = [
    'llama',
    'staunch',
    'zach',
    'joey'
    ]

for animal in animals:
    print(f"{animal.title()} is a great pet.")

print("Some of those animals are meat, " +
    "and some are synthetic. Can you guess which?")
