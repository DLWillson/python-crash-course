import sys

amillion=list(range(1_000_001))

for num in amillion:
    if num < 10 or num > 999_990:
        print(num)
    elif not num % 100_000:
        print( f"{ num / 1_000_000 * 100 }%" )
    elif not num % 1000:
        sys.stdout.write('.')
