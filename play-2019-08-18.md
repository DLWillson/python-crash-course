# complacent comma placement
this:

```python
list = [
    'this',
    'that',
    'the other thing',
]
```

is allowable and better than this:

```python
list = [ 'this', 'that', 'the other thing' ]
```

It's allowable because Guido rocks. It's better because it produces the same desired result, and it's more maintainable.

# lists from strings using string.split()

```python
thingsstring = '''
alice
bob
charlie
dani
eve
'''

thingslist = thingsstring.split('\n')

while '' in thingslist:
    thingslist.remove('')

print(thingslist)
```

# lists from files

NOT using file.readlines()

```python
users = []

with open('users.list') as f:
       for line in f:
          users.append(line.strip())

print(users)
```
