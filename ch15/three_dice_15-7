#!/usr/bin/env python3
# Credit: Based on examples in Python Crash Course by Eric H. Matthes
from plotly.graph_objs import Bar, Layout
from plotly import offline

from die import Die

# Create two D6 dice
die_1 = Die()
die_2 = Die()
die_3 = Die()

# Make some rolls, and store the results in a list
results = []
for roll_num in range(10000):
    result = die_1.roll() + die_2.roll() + die_3.roll()
    results.append(result)
# print(results)

# Analyze the results
frequencies = []
max_result = die_1.num_sides + die_2.num_sides + die_3.num_sides
for value in range(3, max_result+1):
    frequency = results.count(value)
    frequencies.append(frequency)
# print(frequencies)

# Visualize the results
x_values = list(range(3,max_result+1))
data = [Bar(x=x_values,y=frequencies)]
x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequency of Result'}
my_layout = Layout(
    title='Results of rolling 3d6 10000x',
    xaxis=x_axis_config,
    yaxis=y_axis_config
    )
offline.plot({'data': data, 'layout': my_layout}, filename='3d6.html')
