sandwich_orders = [
    'turkey',
    'cheese',
    'chicken',
    'tuna',
    'pastrami',
    'beef',
    'bologna',
    'tomato',
]

completed_sandwiches = []

while len(sandwich_orders):
    sandwich = sandwich_orders.pop()
    print(f'Making a { sandwich } sandwich.')
    completed_sandwiches.append(sandwich)

print("completed_sandwiches:")
for sandwich in completed_sandwiches:
    print('- ' + sandwich)
