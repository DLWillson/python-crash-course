responses = []

polls_to_do = 7
polls_done = 0
while polls_to_do > 0:
    name = input("What's your name? ")
    dream_vaca = input("If you could visit one place in the world, where would you go? ")
    response = {
        'name': name,
        'dream_vaca': dream_vaca,
    }
    responses.append(response)
    polls_done += 1
    polls_to_do -= 1
    print(f"{ polls_done } down. { polls_to_do } to go.")

for response in responses:
    print(f"{ response['name'].title() } would love to go to { response['dream_vaca'].title() }.")
