sandwich_orders = [
    'pastrami',
    'turkey',
    'cheese',
    'pastrami',
    'chicken',
    'tuna',
    'pastrami',
    'beef',
    'bologna',
    'tomato',
]

completed_sandwiches = []

print('The shop has run out of pastrami, folks. Sorry!')
while 'pastrami' in sandwich_orders:
    print('cancelling a pastrami sandwich order...')
    sandwich_orders.remove('pastrami')

while len(sandwich_orders):
    sandwich = sandwich_orders.pop()
    print(f'Making a { sandwich } sandwich...')
    completed_sandwiches.append(sandwich)

print("completed_sandwiches:")
for sandwich in completed_sandwiches:
    print('- ' + sandwich)
