# is it evenly divisible by 10
prompt = "Pick a number and I'll tell you whether it's a multiple of 10\n"
prompt += "or pick a non-number and I'll crash out with an error\n"
prompt += "or pick 'quit' and I'll exit gracefully.\n"
print(prompt)

pick = ''
picks = []

while 1:
    pick = input("# | NaN | quit: ")
    picks.append(pick)
    if pick == 'quit':
        break
    if int(pick) % 10:
        print(f"{ pick } is not evenly divisible by 10.")
    else:
        print(f"{ pick } is evenly divisible by 10.")

if len(picks) > 1:
    print(f"You picked { len(picks) } picks:\n{ picks }")
else:
    print(f"You picked 1 pick:['quit']")
