while 1:
    
    party_size = input("How may in your party, cupcake? ")

    if int(party_size) > 4:
        print("Thaaaat's gonna take a minute or 30. Please sign the register.")
    elif int(party_size) > 0:
        print("Your table is ready! Right this way...")
    elif party_size == 'quit':
        exit()
    else:
        print("Is that even a number?")

# How do I control for non-numbers?
