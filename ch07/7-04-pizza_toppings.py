# pizza toppings
print("Enter pizza toppings, one at a time.")
print("When you've entered all the toppings you desire, enter 'quit'.\n")

toppings = []

while 1:
    topping = input("topping or 'quit': ")
    if topping == 'quit':
        break
    elif topping:
        toppings.append(topping)
    else:
        print("What was that?")

if len(toppings) > 1:
    print(f"You picked { len(toppings) } toppings:\n{ toppings }")
else:
    print(f"You ordered a simple { toppings[0] } pizza.")
