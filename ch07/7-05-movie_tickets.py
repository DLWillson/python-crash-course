# <3 is free
# 3-12 is $10
# > 12 is $15
print("Welcome to the fictional movie theatre!")

while 1:
    age = input("How old are you? ")

    if age:
        age = int(age)
    else:
        print("That's OK, you don't have to say.")
        print("I'll guess you're... 15 years old.")
        age = 15


    if age < 3:
        ticket_cost = 'free'
    elif age > 12:
        ticket_cost = '$15'
    else:
        ticket_cost = '$10'

    print(f"Your ticket is { ticket_cost }.")
