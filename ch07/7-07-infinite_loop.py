import sys
import time
loops = 0

while 1:
    sys.stdout.write('.')
    sys.stdout.flush()
    time.sleep(0.1)
    loops += 1
    if not loops % 10:
        sys.stdout.write('\t')
    if not loops % 40:
        sys.stdout.write('\n\a')
