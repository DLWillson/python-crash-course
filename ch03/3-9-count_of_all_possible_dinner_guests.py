dinner_guests=[
'leo tolstoy',
'lysander spooner',
'heather l. willson',
'carla a. willson',
'irene f. bombard',
'david k. willson',
'ethan danger']

dead=[
'leo tolstoy',
'lysander spooner',
'carla a. willson',
'irene f. bombard',
'david k. willson'
]

dinner_guests.extend(dead)

alternates=['troy ridgley',
'aaron brown',
'anthony sayre',
'jeff haemer',
'heather l. willson',
'carla a. willson',
'chris fedde',
'silvia briscoe',
'dave anselmi',
'chris fedde',
'david k. willson',
'mike shoup',
'rich glazier',
'ethan danger']

dinner_guests.extend(alternates)

print(f"sloppy list: {dinner_guests}")

dinner_guests=sorted(set(dinner_guests))

print(f"sorted, uniqued list: {dinner_guests}")
