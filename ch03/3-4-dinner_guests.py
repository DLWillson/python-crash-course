dinner_guests=[
'leo tolstoy',
'lysander spooner',
'heather l. willson',
'carla a. willson',
'irene f. bombard',
'david k. willson',
'ethan danger']

for guest in dinner_guests:
    print(f"Please come to dinner, {guest.title()}")
