dinner_guests=[
'leo tolstoy',
'lysander spooner',
'heather l. willson',
'carla a. willson',
'irene f. bombard',
'david k. willson',
'ethan danger']

dead=[
'leo tolstoy',
'lysander spooner',
'carla a. willson',
'irene f. bombard',
'david k. willson'
]

alternates=['troy ridgley',
'aaron brown',
'anthony sayre',
'jeff haemer',
'heather l. willson',
'carla a. willson',
'chris fedde',
'silvia briscoe',
'dave anselmi',
'chris fedde',
'david k. willson',
'mike shoup',
'rich glazier',
'ethan danger']


for guest in dead:
    print(f"{guest.title()} can't come to dinner, because they're dead, so I'm removing them from the guest list.")
    dinner_guests.remove(guest)

while len(dinner_guests) < 6 and len(alternates):
    first_alternate=alternates[0]
    del alternates[0]
    if first_alternate not in dinner_guests and first_alternate not in dead:
        print(f"adding {first_alternate.title()} to the guest list.")
        dinner_guests.append(first_alternate)

for guest in dinner_guests:
    print(f"Please come to dinner, {guest.title()}")

print("A bigger table appears!")

while len(dinner_guests) < 12 and len(alternates):
    first_alternate=alternates[0]
    del alternates[0]
    if first_alternate not in dinner_guests and first_alternate not in dead:
        print(f"adding {first_alternate.title()} to the guest list.")
        dinner_guests.append(first_alternate)

for guest in dinner_guests:
    print(f"Please come to dinner, {guest.title()}")

print("How about something more intimate?")

while len(dinner_guests) > 2:
    uninvited=dinner_guests.pop()
    print(f"I'm sorry {uninvited.title()}, but I need quiet time.")

while len(dinner_guests) > 0:
    print(f"Please come to small dinner, {dinner_guests[0].title()}.")
    del dinner_guests[0]

for guest in dinner_guests:
    print(f"{guest} is left over.")
