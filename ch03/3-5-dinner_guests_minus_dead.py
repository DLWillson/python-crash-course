dinner_guests=[
'leo tolstoy',
'lysander spooner',
'heather l. willson',
'carla a. willson',
'irene f. bombard',
'david k. willson',
'ethan danger']

dead=[
'leo tolstoy',
'lysander spooner',
'carla a. willson',
'irene f. bombard',
'david k. willson'
]

alternates=['troy ridgley',
'aaron brown',
'anthony sayre',
'jeff haemer',
'heather l. willson',
'carla a. willson',
'chris fedde',
'dave anselmi',
'chris fedde',
'david k. willson',
'mike shoup',
'rich glazier',
'ethan danger']


for guest in dead:
    print(f"{guest.title()} can't come to dinner, because they're dead, so I'm removing them from the guest list.")
    dinner_guests.remove(guest)

while len(dinner_guests) < 6 and len(alternates):
    first_alternate=alternates[0]
    alternates.remove(0)
    if first_alternate not in dinner_guests and first_alternate not in dead:
        dinner_guests.append(first_alternate)

for guest in dinner_guests:
    print(f"Please come to dinner, {guest.title()}")
