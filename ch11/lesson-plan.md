1. If you want people (even you) to enjoy using your things, don't break the things that depend on your things. Test your interfaces off.
2. Add tests as you add functionality.
3. If you must break old functionality, increment your [major version](https://semver.org/)

[Eric's code](https://github.com/ehmatthes/pcc), including the words files, is on GitHub.

To pull Eric's code and words files to your machine, do this:

```bash
git clone https://github.com/ehmatthes/pcc.git
```

[DLWillson's code](https://gitlab.com/DLWillson/python-crash-course), including everything you see here, is on GitLab.

To pull DLW's code to your machine, do this:

```bash
git clone https://gitlab.com/DLWillson/python-crash-course.git
```

Impact matters: Study for impact!

- Best: Create the best code you can. Pretend you're going to re-use your code.
- Almost best: Transliterate from the book, typing everything out and making mods.
- Next almost: Copy solutions and modify them.
- Not useless: Copy solutions. Run and read them until you kinda understand.
- Why bother:  Read, copy, run.

---

Write a lib that rolls dice
