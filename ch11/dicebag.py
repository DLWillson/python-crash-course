# Author: David L. Willson
# License: GPL for code, CC BY SA for art, or public domain for trivial works
# should take natural seeming dice expressions, like:
# roll(5)      # rolls 5 "normal" dice
# roll()       # should this roll a normal die, or re-roll the last die?
# roll("d20")  # roll a 20-sided die
# roll("2d12") # roll 2 12-sided dice
from random import randint
from pprint import pprint

def roll(arg):
    try:
        int(arg)
    except:
        args=arg.split('d')
        pprint(args)
        return args
    else:
        rolls=[]
        while len(rolls) < arg:
            rolls.append(randint(1,6))
        pprint(rolls)
        return rolls

# normal_die=Die() # this uses the default value of six
# ten_sider=Die(10)
# twenty_sider=Die(20)
#
# rolls={
#     'sixxer': [],
#     'tenner': [],
#     'twenty': [],
# }
#
# for roll in range(10):
#     rolls['sixxer'].append(normal_die.roll())
#     rolls['tenner'].append(ten_sider.roll())
#     rolls['twenty'].append(twenty_sider.roll())
#
# pprint(rolls)
# return rolls
