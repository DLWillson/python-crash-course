class Employee:
    """Represents an Employee."""

    def __init__(self,first_name,last_name,salary):
        """Store info about the Employee."""
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary

    def give_raise(self, amount=5000):
        """Give a fella a raise."""
        old_salary=self.salary
        self.salary += amount
        print(f"salary was {old_salary} and is now {self.salary}")
