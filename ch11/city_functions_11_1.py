def format_city_country(city, country):
    """Takes 'city' and 'country' and returns 'City, Country'"""
    formatted_city_country = city.title() + ", " + country.title()
    return formatted_city_country
