#!/usr/bin/env python3
# Author: David L. Willson
# License: GPL for code, CC BY SA for art, or public domain for trivial works
def get_formatted_name(first, last, middle=''):
    """Generate a title-cased name"""
    if middle:
        full_name = first + " " + middle + " " + last
    else:
        full_name = first + " " + last
    return full_name.title()
