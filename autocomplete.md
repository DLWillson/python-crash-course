On Fedora and Ubuntu, interactive python3 sessions had auto-complete. H

```
$ python3
Python 3.6.8 (default, Jan 14 2019, 11:02:34) 
[GCC 8.0.1 20180414 (experimental) [trunk revision 259383]] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import time
>>> time. # after typing dot I tapped tab twice to get a list of possible completions
time.CLOCK_MONOTONIC           time.clock(                    time.gmtime(                   time.strftime(
time.CLOCK_MONOTONIC_RAW       time.clock_getres(             time.localtime(                time.strptime(
time.CLOCK_PROCESS_CPUTIME_ID  time.clock_gettime(            time.mktime(                   time.struct_time(
time.CLOCK_REALTIME            time.clock_settime(            time.monotonic(                time.time(
time.CLOCK_THREAD_CPUTIME_ID   time.ctime(                    time.perf_counter(             time.timezone
time.altzone                   time.daylight                  time.process_time(             time.tzname
time.asctime(                  time.get_clock_info(           time.sleep(                    time.tzset(
>>> time.clock() 
0.0628
>>> time.time()
1563498889.3326383
>>> time.timezone
0
>>> exit()
```

Here are some more examples.

```
$ python3
>>> import os
>>> os. # two tabs
>>> import sys
>>> sys. # two tabs
>>> ran # one tab completes to range(
>>> pri # one tab completes to print(
```

