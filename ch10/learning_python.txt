In Python, you can define and set variables.
In Python, you can define and populate functions.
In Python, you can use functions inherent to data objects.
In Python, you can use structure data any way you like.
In Python, you can create classes as templates for objects.
In Python, you can create objects that reflect real and imaginary things.
