"""Defines URL patterns for learning_logs"""
from django.urls import path

from . import views

app_name = 'articles'

urlpatterns = [
    # home page
    path('', views.index, name='index'),
    path('new/', views.new, name='new'),
    path('edit/<int:article_id>/', views.edit, name='edit'),
]
