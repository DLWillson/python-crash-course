from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):
    """A blog article"""
    title = models.CharField(max_length=256)
    date_added = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    author = models.ForeignKey(User,on_delete=models.CASCADE)
    
    def __str__(self):
        """string representation of the model"""
        return self.title
