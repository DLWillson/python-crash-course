from django.db import models

class Pizza(models.Model):
    """A kind of pizza, like 'Hawaiian' or 'Meat Lovers'"""
    name = models.CharField(max_length=128)

class Topping(models.Model):
    """A topping for pizza, like 'Sausage' or 'Mushroom'"""
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
