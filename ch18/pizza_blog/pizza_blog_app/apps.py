from django.apps import AppConfig


class PizzaBlogAppConfig(AppConfig):
    name = 'pizza_blog_app'
