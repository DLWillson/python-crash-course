from django.shortcuts import render

from .models import Pizza

def index(request):
    """The homepage for Pizza Blog App"""
    return render(request, 'pizza_blog_app/index.html')

def pizzas(request):
    """Show all the pizzas"""
    pizzas = Pizza.objects.order_by('name')
    context = {'pizzas': pizzas}
    return render(request, 'pizza_blog_app/pizzas.html', context)

def pizza(request, pizza_id):
    """Show the toppings for a pizza"""
    pizza = Pizza.objects.get(id=pizza_id)
    toppings = pizza.topping_set.order_by('name')
    context = {'pizza': pizza, 'toppings': toppings}
    return render(request, 'pizza_blog_app/pizza.html', context)
