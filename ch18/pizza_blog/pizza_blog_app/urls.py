"""URL patters for pizza_blog_app"""
from django.urls import path
from . import views
app_name = 'pizza_blog_app'
urlpatterns = [
    # home page
    path('', views.index, name='index'),
    # pizzas list
    path('pizzas/', views.pizzas, name='pizzas'),
    # pizza topping list
    path('pizza/<int:pizza_id>/', views.pizza, name='pizza'),
]
