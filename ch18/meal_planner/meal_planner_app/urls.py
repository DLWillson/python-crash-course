"""Defines the URL patterns for Meal Planner"""
from django.urls import path
from . import views
app_name = 'meal_planner_app'
urlpatterns = [
    # home page
    path('', views.index, name='index'),
]
