import pygame
from pygame.sprite import Sprite
import sys
from random import randint

class Raindrop(Sprite):
    """It's a raindrop. It falls. And, it has a raindrop shape."""

    def __init__(self, shower):
        super().__init__()
        self.screen = shower.screen
        self.screen_rect = self.screen.get_rect()
        self.image = pygame.image.load('images/raindrop.bmp')
        self.rect  = self.image.get_rect()
        self.image.set_colorkey((0,0,0))

        self.rect.midbottom = (randint(100,self.screen_rect.right-100),0)

    def update(self):
        """Update the falling raindrop's position"""
        self.rect.y += 1 + randint(0,3)
        self.rect.x += 0 + randint(-2,2)

    def show(self):
        self.screen.blit(self.image,self.rect)
