# Chapter 2: Variables and Simple Data Types

[Chapter 2 Lesson Plan by DLW](https://gitlab.com/DLWillson/python-crash-course/-/blob/master/ch02/lesson-plan.md)

## Seven Most Important Things

* Variables are values called by name, so that the value can be changed as needed, hence the term "variable".

* Constants (static variables, which is an oxymoron, but never mind that) are named in all caps, by convention.

  ```python
  PI=3.1415926535
  ```

  Be aware that the convention is not enforced by the interpreter!

  ```python
  PI="3ish OK?"
  ```

* Interactive Python is a *great* way to explore!
* Strings are objects with attributes and methods.
* Methods work on string literals, but method auto-complete only works on variables.
* f-strings allow variable interpolation

* Shebangs are good and file extensions are bad.
  - Python3 script shebang:

    `#!/usr/bin/env python3`

  - Python2 script shebang:

    `#!/usr/bin/env python2`

  - Version-agnostic script shebang: (works correctly in any Python)

    `#!/usr/bin/env python`

  - The user doesn't care what language you used, and shouldn't have to. `sort some_list` reads better than `sort.py some_list` or `sort.sh some_list`.
  - The user definitely doesn't care what *version* of the language you used. Looking at you, `.py3`


## Exercise/s

(for Demo and Pair (or Mob) Practice)

### Interactive Python and auto-complete

Start an interactive python session.

```bash
# /usr/bin/env python3
# or, just
python3
```

Note that the return of bare values is the value.

Note that methods can be invoked on static strings.

```
$ python3
Python 3.8.2 (default, Jul 16 2020, 14:00:26)
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 3
3
>>> twelve
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'twelve' is not defined
>>> "twelve"
'twelve'
>>> "twelve".upper()
'TWELVE'
>>> "twelve".title()
'Twelve'
```

Note that you *can't* auto-complete methods on static strings.

```
>>> type("twelve")
<class 'str'>
>>> "twelve".
```

... and <TAB> like a monkey trying to open a coconut.
But, you *can* auto-complete methods on variables of type 'str'.

```
>>> myvar="twelve"
>>> type(myvar)
<class 'str'>
>>> myvar.
myvar.capitalize(    myvar.isdigit(       myvar.rfind(
myvar.casefold(      myvar.isidentifier(  myvar.rindex(
myvar.center(        myvar.islower(       myvar.rjust(
myvar.count(         myvar.isnumeric(     myvar.rpartition(
myvar.encode(        myvar.isprintable(   myvar.rsplit(
myvar.endswith(      myvar.isspace(       myvar.rstrip(
myvar.expandtabs(    myvar.istitle(       myvar.split(
myvar.find(          myvar.isupper(       myvar.splitlines(
myvar.format(        myvar.join(          myvar.startswith(
myvar.format_map(    myvar.ljust(         myvar.strip(
myvar.index(         myvar.lower(         myvar.swapcase(
myvar.isalnum(       myvar.lstrip(        myvar.title(
myvar.isalpha(       myvar.maketrans(     myvar.translate(
myvar.isascii(       myvar.partition(     myvar.upper(
myvar.isdecimal(     myvar.replace(       myvar.zfill(
```

    <Ctrl>+C or hold <Backspace> to erase an incomplete and unwanted line.

    <Ctrl>+D to exit.

Re-invoke Python3, hit <Up> a few times and <Down>. Notice that your history was preserved between sessions.

### shebang and chmod

test, change, repeat

```bash
echo '#!/usr/bin/env python3' > hello_world
chmod +x hello_world
./hello_world
echo 'print("Hello, World!")' >> hello_world
./hello_world
echo 'print("Hello again, World!")' >> hello_world
./hello_world
```

### Edit it. Save it. Run it. Repeat.

Use your favorite editor to change it.

```bash
idle hello_world
# or
# vim hello_world
# atom hello_world
# gedit hello_world
# sublime hello_world
# notepad hello_world
# vscodium hello_world
```

```
$ cat hello_world
#!/usr/bin/env python3
print("Hello, World!")
print("Hello again, World!")
world="Earth"
print(f"Hello {world}, you're my world.")
```

## The cutting room floor:

* Linux is butter. Windows is ... not butter.
  - Use Linux
  - Or a Linux VM
  - Or a Linux Terminal Server
  - Or CygWin
  - Or WSL
  - Or just go sadly on with no butter
* The Zen of Python
* Twelve-Factor Architecture
* The Agile Manifesto
* The Gnu Manifesto
* Escape sequences like \n and \t can be used to put whitespace in strings.
