current_users_string='''
Ocie
Vicki
Nerissa
Valeria
Paulette
Genesis
Delena
Many
Leoma
Sherice
Sybil
Shantel
Hattie
Valeri
Senaida
Lien
Theodora
Sarita
Deidra
Bettyann
Kathe
Brittny
Isabella
Sophia
Charlotte
Mia
Deneen
Jenice
Adria
Karri
Catherine
Bettina
Athena
Marcelina
Trista
Barrie
Chelsey
Debroah
Yasuko
Abigail
Julietta
Leonarda
Lecia
Regenia
Lidia
Nenita
Daniele
Felicia
Gaye
Lissa
Margene
Consuelo
Tamie
Loria
Yvonne
Tona
Katy
Latonia
Lorriane
Lashawn
Shella
'''

new_users_string='''
Isobel
Willia
Shaunte
Rhonda
Valorie
Jene
Fabiola
Albertina
Isidra
Cherri
Isabella
Kellie
Emily
Reina
Susana
Susy
Lucy
Maryln
Joaquina
Darcy
Keiko
Ozell
Kymberly
Lauri
Nelia
Dinorah
Gina
Emma
Cindy
Olivia
Yajaira
Piper
Lizzette
Mai
Francina
Rosette
Fernanda
Darline
Ferne
Charlotte
Francisca
Diedra
Lekisha
Gladys
Roselee
Conchita
Brook
Marvella
Janel
Sophia
Katia
Charlesetta
Halley
Abigail
Lizeth
Chantel
Maureen
Mia
'''

# make a clean, sorted, unique, lowercase list from a newline delimited string
current_users_list = current_users_string.split('\n')
current_users_list = [user.strip() for user in current_users_list ]
while '' in current_users_list:
    current_users_list.remove('')
print(f"This system currently has {len(current_users_list)} users.")
lowercase_current_users = [ user.lower() for user in current_users_list ]
#print(lowercase_current_users)
lowercase_current_users = list( set( lowercase_current_users ) )
lowercase_current_users.sort()
#print(lowercase_current_users)

new_applicants_username_proposals = new_users_string.split('\n')

for proposal in new_applicants_username_proposals:
    proposal = proposal.strip()
    if proposal == '':
        print("Sorry, we don't allow that username.")
    elif proposal.lower() in lowercase_current_users:
        print(f"Sorry, the username {proposal} is already taken.")
    else:
        print(f"Welcome aboard, {proposal}!")
        current_users_list.append(proposal)
        current_users_list.sort()

print(f"This system currently has {len(current_users_list)} users.")
