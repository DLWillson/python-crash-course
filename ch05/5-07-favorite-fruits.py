my_favorite_fruits = [
    'tomato',
    'grape',
    'lemon',
    'blueberry',
    'raspberry',
    ]

guesses = [
    'apple',
    'grape',
    'banana',
    'blueberry',
    'kiwi',
    'orange',
    'raspberry',
    ]

for guess in guesses:
    if guess in my_favorite_fruits:
        print(f"Yes, {guess} *is* one of my favorite fruits!")
    else:
        print(f"No, {guess} is not one of my favorite fruits.")
