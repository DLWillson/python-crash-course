if "Ben" == 'Ben':
    print("Yay! Quote style doesn't matter to stringly equality")
else:
    print("Boo... Silly me, I guess.")

if "Ben" == "ben":
    print("Boo! Silly me, I guess.")
else:
    print("Yay! String comparison is case sensitive, as I expected!")

if "Ben".lower() == 'BEN'.lower() and "Ben".upper() == 'BEN'.upper():
    print("Yay! upper() or lower() both sides to make comp case-insensitive.")
else:
    print("Boo... Silly me, I guess.")

list=[
    'this',
    'that',
    'the other thing'
    ]

if 'this' in list:
    print("Yay! 'this' made the list!")
else:
    print("Boo... Silly me, I guess.")

if 'everything' not in list:
    print("Yay! 'everything' isn't in the list.")
else:
    print("Boo... Silly me, I guess.")
