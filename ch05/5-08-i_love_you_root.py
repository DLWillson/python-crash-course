usernames = [
    'root',
    'dlwillson',
    'jimidak',
    'captain_arugula',
    'npug',
    ]

for username in usernames:
    if username != 'root':
        print(f"Welcome to the system, {username}.")
    else:
        print(f"You're a super-user, {username}! I am yours to command!")
        print(f"# ")
