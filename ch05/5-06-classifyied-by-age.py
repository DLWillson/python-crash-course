# load up an array with 100 random ages
import random
for person in range(100):
    age=random.randint(1,100)
    if age < 2:
        age_name='baby'
    elif age < 4:
        age_name='toddler'
    elif age < 13:
        age_name='kid'
    elif age < 20:
        age_name='teenager'
    elif age < 65:
        age_name='adult'
    else:
        age_name='elder'
    if age_name=='adult' or age_name=='elder':
        article='an'
    else:
        article='a'
    print(f"At {age} years old, person is {article} {age_name}.")
