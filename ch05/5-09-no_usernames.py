unusued_usernames="""
usernames = [
    'root',
    'dlwillson',
    'jimidak',
    'captain_arugula',
    'npug',
    ]
"""

usernames=[]

if not usernames:
    print("The list of users is empty!")
    exit()

for username in usernames:
    if username != 'root':
        print(f"Welcome to the system, {username}.")
    else:
        print(f"You're a super-user, {username}! I am yours to command!")
        print(f"# ")
