for n in range(134):

    # "three thousand, four hundred, sixty eighth"
    # AFAICT, only the last two digits matter to the ordinal
    # ords: ordinal relevant digits
    ords = n % 100

    # all ordinals from 4th through 20th end in th
    if ords > 3 and ords < 21:
        ordinal=str(n)+'th'
    # other numbers ending in one are umpty-firsts
    elif ords % 10 == 1:
        ordinal=str(n)+'st'
    # other numbers ending in two are umpty-seconds
    elif ords % 10 == 2:
        ordinal=str(n)+'nd'
    # other numbers ending in three are umpty-thirds
    elif ords % 10 == 3:
        ordinal=str(n)+'rd'
    # everything is else is umpty-whatever-th
    else:
        ordinal=str(n)+'th'

    print(f"{n} is the {ordinal} number.")

print("but ONE is the LONELIEST number")
