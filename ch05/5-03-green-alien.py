colors=[
    'green',
    'yellow',
    'red'
    ]

for color in colors:
    print(f"The alien that was just shot down is {color}.")
    if color == "green":
        print("The player earns 5 points!")
    else:
        print("Nothing particularly interesting happens.")
