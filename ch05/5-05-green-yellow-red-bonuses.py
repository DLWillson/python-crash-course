colors=[
    'green',
    'yellow',
    'red'
    ]

for color in colors:
    print(f"The alien that was just shot down is {color}.")
    if color == "green":
        print("The player earns 5 points!")
    elif color == "yellow":
        print("The player earns 10 points!")
    elif color == "red":
        print("The player earns 15 points!")
    else:
        print("That's a strange color! Nothing interesting happens...")
