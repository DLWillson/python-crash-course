if 4 < 8:
    print("Yay! 4 is less than 8, as I expected!")
else:
    print("Boo... I thought 4 was less than 8. Silly me, I guess.")

if 1 != 1:
    print("Boo! I thought 1 was equal to 1. Silly me, I guess.")
else:
    print("Yay! 1 is equal to 1, as I expected!")

if True and True:
    print("Yay! True and True is True, as I expected!")
else:
    print("Boo... I thought True and True would be True.")

if True and False:
    print("Boo... I thought True and False would be False.")
else:
    print("Yay! True and False is False, as I expected!")

if False and True:
    print("Boo... I thought False and True would be False.")
else:
    print("Yay! False and True is False, as I expected!")

if False and False:
    print("Boo... I thought False and False would be False.")
else:
    print("Yay! False and False is False, as I expected!")

if True or True:
    print("Yay! True or True is True, as I expected!")
else:
    print("Boo... I thought True or True would be True.")

if True or False:
    print("Yay! True or False is True, as I expected!")
else:
    print("Boo... I thought True or False would be True.")

if False or True:
    print("Yay! False or True is True, as I expected!")
else:
    print("Boo... I thought False or True would be True.")

if False or False:
    print("Boo... I thought False or False would be False.")
else:
    print("Yay! False or False is False, as I expected!")
