programmers = [
    'David',
    'Rich',
    'Corey',
    'Silvia',
    'Shannon',
    'Brad',
    'Desiree',
    'RJ',
    ]

favorite_languages = {
    'David':'C',
    'Rich':'Python',
    'Corey':'PHP',
    'Brad':'Perl',
    'RJ':'R',
    }

for p in programmers:
    if p in favorite_languages.keys():
        print(f"Thanks for taking the survey, {p}. We have recorded your favorite language as {favorite_languages[p]}.")
    else:
        print(f"{p}, you should take our favorite programming language survey.")
        
