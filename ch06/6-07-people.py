people = {
    'heather': {
        'hair': 'brown',
        'city': 'denver',
        'eyes': 'green',
        'first_name': 'heather',
        'last_name': 'willson',
        'car': 'camry',
    },
    'david': {
        'hair': 'brown',
        'city': 'denver',
        'age': '52',
        'eyes': 'hazel',
        'height': '6\'1"',
        'weight': '197',
        'car': 'big green van',
        'fav_prog_lang': 'C',
        },
    'davey': {
        'hair': 'brown',
        'city': 'denver',
        'age': '29',
        'eyes': 'brown',
        'height': '6\'2"',
        'pet': { 'cat': 'eep', },
        },
    'evan': {
        'hair': 'blond',
        'city': 'Highlands Ranch',
        'age' : 22,
        'height': '6\''
        },
    }

# create the set of unique keys for people
all_possible_attributes = set()
for person in people:
    for k in people[person].keys():
        all_possible_attributes.add(k)

# show each person's keys, present and missing
for person in people:
    print(f"*{ '-' * 79 }")
    print(f"| {person} :")
    print(f"*{'-' * (len(person)+3)}")
    for attribute in sorted(all_possible_attributes):
        print(f"|    {attribute} is {people[person].get(attribute, 'unknown')}.")
print(f"*{ '-' * 79 }")
