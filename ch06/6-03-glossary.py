glossary = {
    'list':'A traditional array',
    'dictionary':'a sequence of key-value pairs like a list, but the keys are words, rather than sequential numbers.',
    'function':'a discrete, re-usable chunk of code that takes arguments, may take input or give output, and may return a value.',
    'modulus':'the "remainder" of a division operation',
    'loop':'a chunk of code that is executed repeatedly while a condition lasts or until it expires',
    }

for k,v in glossary.items():
    print(f"{k.upper()}:\n{v}\n")
