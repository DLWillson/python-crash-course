pets = {
    'steve': {
        'type': 'dog',
        'owner': 'the wicker family',
        },
    'eep': {
        'type': 'cat',
        'owner': 'davey',
        },
    'joey': {
        'type': 'dog',
        'owner': 'the willson family',
        },
    'zach': {
        'type': 'dog',
        'owner': 'the willson family',
        },
    'white bird': {
        'type': 'bird',
        'owner': 'davey',
        },
    'licorice': {
        'type': 'cat',
        'owner': 'the mahoney family',
        },
    'butterbean': {
        'type': 'dog',
        'owner': 'the mahoney family',
        },
    }

for pet in pets:
    print(f"{pet.title()} is a {pets[pet]['type']} that belongs to {pets[pet]['owner']}.")
