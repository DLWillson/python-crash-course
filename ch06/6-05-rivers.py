rivers = {
        'nile':'egypt',
        'mississippi':'the united states',
        'thames':'the united kingdom',
        'danube':'europe',
        }

for r,c in rivers.items():
    print(f"The {r.title()} river is in {c.title()}.")

for r in rivers.keys():
    print(r)

for c in rivers.values():
    print(c)
