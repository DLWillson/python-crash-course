glossary = {
    'list':'A traditional array',
    'dictionary':'a sequence of key-value pairs like a list, but the keys are words, rather than sequential numbers.',
    'function':'a discrete, re-usable chunk of code that takes arguments, may take input or give output, and may return a value.',
    'modulus':'the "remainder" of a division operation',
    'loop':'a chunk of code that is executed repeatedly while a condition lasts or until it expires',
    'string':'a list of characters and an object with useful methods',
    'assignment':'an = causes the value of the right-side expression to be assigned to the left-side variable',
    'equality': 'a == causes the right and left hand expression to be evaluated for equality and returns true when they\'re equal and false otherwise',
    'import':'a keyword which includes a library of code into the current program, making it\'s classes and functions available',
    'return':'a keyword which causes a function to exit and send back the value of the following expression',
    }

for k,v in glossary.items():
    print(f"{k.upper()}:\n{v}\n")
