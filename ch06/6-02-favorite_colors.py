favorite_colors = {
    'evan':'red',
    'davey':'green',
    'heather':'green',
    'david':'gold',
    }

for key,value in favorite_colors.items():
    print(f"{key.title()}'s favorite color is {value}.")
