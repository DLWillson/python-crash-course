heather = {
    'hair': 'brown',
    'city': 'denver',
    'age': '40-something',
    'eyes': 'green',
    'first_name': 'heather',
    'last_name': 'willson'
    }

print(heather)

print(f"Heather's car: { heather.get('car', 'Heather has no car.')}")
