favorite_places = {
    'heather': { 'free spirit spheres', 'plant city', 'denver', 'sanibel', 'maui', },
    'davey': { 'gaming goat', 'locavore', 'nixons', 'enchanted grounds', },
    'david': { 'nanaimo', 'livingston county', 'vegreville', 'denver', 'snow mountain ranch', },
    'evan': { 'sheep mountain', 'buffalo wild wings', 'Orlando', 'idaho springs', },
    }

for person in favorite_places:
    print(f"{person.title()}'s favorite places are:")
    for place in favorite_places[person]:
        print(f"- {place.title()}")
        
