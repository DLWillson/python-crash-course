cities = {
    'rochester, new york': {
        'population': '206k',
        'country': 'united states',
        'fact': 'is the home of Kodak and Xerox and the Lilac Festival',
        },
    'denver, colorado': {
        'population': '716k',
        'country': 'united states',
        'fact': 'started on livestock, saloons, and gambling',
        },
    'islamabad': {
        'population': '1015k',
        'country': 'pakistan',
        'fact': 'is a planned city built in 1960 to replace Karachi as the capital of Pakistan',
        },
    }

for city in cities:
    print(f"{city.title()} in {cities[city]['country'].title()} has a population of {cities[city]['population']} and {cities[city]['fact']}.")
