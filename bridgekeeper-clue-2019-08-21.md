# A Clue for a BridgeKeeper

2019-08-21

```
[dlwillson@DLW-WS0 ~]$ python3
Python 3.7.4 (default, Jul  9 2019, 16:32:37)
[GCC 9.1.1 20190503 (Red Hat 9.1.1-1)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> good_answer = 'I seek the grail!'
>>> bad_answer = 'I seek tacos!'
>>> answer = good_answer
>>> if 'grail' in answer:
...     print('correct')
... else:
...     print('fling')
...
correct
>>> answer = bad_answer
>>> if 'grail' in answer:
...     print('correct')
... else:
...     print('fling')
...
fling
>>>
[dlwillson@DLW-WS0 ~]$
```

```python
good_answer = 'I seek the grail!'
bad_answer = 'I seek tacos!'

answer = good_answer
if 'grail' in answer:
    print('correct')
else:
    print('fling')


answer = bad_answer
if 'grail' in answer:
    print('correct')
else:
    print('fling')
```
